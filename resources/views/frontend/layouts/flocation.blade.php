@extends ('frontend.master')
@section('content')
    <section>
        <div class="container">
            <div class="row">

                @foreach($datas as $data)
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">{{$data->name}}</h5>
                                <b> Mobile:</b>
                                <p class="card-text">{{$data->mobile}}</p>
                                <b>Address:</b>
                                <p class="card-text">{{$data->address}}</p>
                                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@stop
