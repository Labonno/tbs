@extends ('frontend.master')

@section('content')

<h1>Ticket Details</h1> 

</button> <br><br>  

        
 <div class="table-responsive" >
    <table class="table table-striped" id="orderTable">
      <thead>
        <tr>
          <th>From</th>
          <th>To</th>
          <th>Number of Seats</th>
          <th>Total Amount</th>
          <th>time</th>
          <th>Date</th>                                                
       </tr>
      </thead>

      <tbody>
             @foreach($buses as $key=>$bus)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$bus->coach_id}}</td>
            <td>{{$bus->route_id}}</td>
            <td>{{$bus->reporting}}</td>
            <td>{{$bus->depature}}</td>
            <td>{{$bus->boarding}}</td>
            <td>{{$bus->coach_type}}</td>
            <td>{{$bus->time}}</td>
            <td>{{$bus->price}}</td>
          </tr>
            @endforeach
      </tbody>
    </table>



@stop
