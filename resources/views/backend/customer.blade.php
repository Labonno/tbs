@extends('backend.master')

@section('content')



<h1>Customer Details</h1> 
<br><br>         
 <div class="table-responsive" >
                    <table class="table table-striped" id="orderTable">
                      <thead>
                        <tr>
                          <th>Serial</th>
                          <th>First Name</th>
                          <th>Email</th>
                          <th>Address</th>
                          <th>Mobile No</th>
                          <th></th>                                                   
                       </tr>
                      </thead>
                        <tbody>
                        <?PHP $i=1;?>
                        @foreach($datas as $key=>$data)
                            <tr>
                                <td>{{$i++}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->email}}</td>
                                <td>{{$data->address}}</td>
                                <td>{{$data->contact_num}}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
</div>

@endsection
