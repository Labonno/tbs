@extends('backend.master')

@section('content')

@extends('backend.master')

@section('content')

  <style>
  h1{
    text-align: center;
  };
  
</style>

<!-- Button trigger modal -->

<h1>Booking Details</h1> 
        
 <div class="table-responsive" >
    <table class="table table-striped" id="orderTable">
      <thead>
        <tr>
          <th>Serial</th>
          <th>User Name</th>
          <th>Coach Number</th>
          <th>Unit Price</th>
          <th>Seat</th>
          <th>Quantity</th>
          <th>Total Amount</th>
          <th>Transaction Id</th>                                                
       </tr>
      </thead>
      <tbody>
             @foreach($data as $key=>$bus)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$bus->user->name}}</td>
            <td>{{$bus->coach_id}}</td>
            <td>{{$bus->booking->unit_price}}</td>
            <td>{{$bus->seat}}</td>
            <td>{{$bus->booking->quantity}}</td>
            <td>{{$bus->booking->total_amount}}</td>
            <td>{{$bus->booking->transaction_id}}</td>
          </tr>
            @endforeach
      </tbody>
    </table>
</div>



@endsection

@section('script')
<script>
  $(document).ready(function(){
    $('#orderTable').DataTable();
  });

</script>
@endsection

