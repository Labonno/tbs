<?php $__env->startSection('content'); ?>
<div class="row">
<div class="col-md-3">
	
</div>
<div class="col-md-6">
<form action="<?php echo e(route('show_login')); ?>" method="POST" >
            <?php echo csrf_field(); ?>
            <div class="form-group">
              <label for="email">E-mail</label>
              <input class="form-control" required type="email" name="email" placeholder="email">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input class="form-control" type="password" required name="password" placeholder="password">
            </div>

            <div class="modal-footer">
              <button class="btn btn-secondary" data-dismiss="modal">Close</button>
              <button class="btn btn-warning" type="submit">Login</button>

            </div>

          </form>	
</div>
<div class="col-md-3">
	
</div>
</div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\localhost\booking_system\resources\views/frontend/login.blade.php ENDPATH**/ ?>