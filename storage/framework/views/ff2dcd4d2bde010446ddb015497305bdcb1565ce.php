<?php $__env->startSection('content'); ?>



<h1>Customer Details</h1> 
<br><br>         
 <div class="table-responsive" >
                    <table class="table table-striped" id="orderTable">
                      <thead>
                        <tr>
                          <th>Serial</th>
                          <th>First Name</th>
                          <th>Email</th>
                          <th>Address</th>
                          <th>Mobile No</th>
                          <th></th>                                                   
                       </tr>
                      </thead>
                        <tbody>
                        <?PHP $i=1;?>
                        <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($i++); ?></td>
                                <td><?php echo e($data->name); ?></td>
                                <td><?php echo e($data->email); ?></td>
                                <td><?php echo e($data->address); ?></td>
                                <td><?php echo e($data->contact_num); ?></td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
</div>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('backend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/Backend/customer.blade.php ENDPATH**/ ?>