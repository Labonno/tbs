<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>
<table class="table table-bordered">
    <tr>
        <td>Name: <?php echo e($downloadPdf->user->name); ?></td>
        <td>Phone: <?php echo e($downloadPdf->user->contact_num); ?></td>
    </tr>
    <tr>
        <td>From: <?php echo e($downloadPdf->coach->routelocation->from); ?></td>
        <td>To: <?php echo e($downloadPdf->coach->routelocation->to); ?></td>
    </tr>
    <tr>
        <td>Coach Number: <?php echo e($downloadPdf->coach_id); ?></td>
        <td>Price: <?php echo e($downloadPdf->booking->unit_price); ?></td>
    </tr>
    <tr>
        <td>Seat No: <?php echo e($downloadPdf->seat); ?></td>
    </tr>
    <tr>
        <td>Quantity: <?php echo e($downloadPdf->booking->quantity); ?></td>
        <td>Total: <?php echo e($downloadPdf->booking->total_amount); ?></td>
    </tr>
</table>
</body>
</html>
<?php /**PATH E:\localhost\bookingsystem\resources\views/frontend/print/pdfTicket.blade.php ENDPATH**/ ?>