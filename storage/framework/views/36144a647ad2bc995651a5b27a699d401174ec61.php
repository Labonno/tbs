<?php $__env->startSection('content'); ?>
<h1>User Details</h1> 
<br><br>         
 <div class="table-responsive" >
                    <table class="table table-striped" id="orderTable">
                      <thead>
                        <tr>
                          <th>Serial</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th>Contact No</th>
                          <th></th>                                                   
                       </tr>
                      </thead>                      
                      <tbody>
                             <?php $__currentLoopData = $user; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                          <tr>
                            <td><?php echo e($key+1); ?></td>
                            <td><?php echo e($user->name); ?></td>
                            <td><?php echo e($user->address); ?></td>
                            <td><?php echo e($user->contact_num); ?></td>
                          </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                      </tbody>
                    </table>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('backend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\localhost\booking_system\resources\views/Backend/partials/user.blade.php ENDPATH**/ ?>