<?php $__env->startSection('content'); ?>
    <section>
        <div class="container">
            <div class="row">

                <?php $__currentLoopData = $datas; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $data): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="col-sm-6">
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title"><?php echo e($data->name); ?></h5>
                                <b> Mobile:</b>
                                <p class="card-text"><?php echo e($data->mobile); ?></p>
                                <b>Address:</b>
                                <p class="card-text"><?php echo e($data->address); ?></p>
                                <!-- <a href="#" class="btn btn-primary">Go somewhere</a> -->
                            </div>
                        </div>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </div>
        </div>
    </section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('frontend.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\booking-system\resources\views/frontend/layouts/flocation.blade.php ENDPATH**/ ?>