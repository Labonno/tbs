<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
  protected $fillable=['name','email','password','address','contact_num'];
}
