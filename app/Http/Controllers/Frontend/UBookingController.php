<?php

namespace App\Http\Controllers\Frontend;

use App\models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Booking;
use App\models\Location;
use App\models\Bus;
use App\models\Routelocation;
use Session;
//use PDF;
use Barryvdh\DomPDF\Facade as PDF;

class UBookingController extends Controller
{
   public function ubooking()
   {
      // $userId=auth()->user()->id;
      // $data=Reservation::where('user_id', $userId)->with('booking')->with('user')->with('coach')->latest()->get();
      $locations= Location::all();
      return view('frontend.layouts.ubooking',compact('locations', 'data'));
   }

   
   public function ubookingPost(Request $request)
   {
       $validatedData = $request->validate([
           'pick_point' => 'required',
           'drop_point' => 'required',
           'date' => 'required',
           'time' => 'required',
           'coach_type' => 'required',
       ]);
        //dd($request->all());
        $date=$request->date;
        $bookingDate=Session::put('bookingdate',$date);
        //dd($bookingDate);
    
  		//$route_id=get route id from route table using Route model, where location from and location to came from user form.

    $buses='';
    //routeslocations matches with from&to....
    $route=Routelocation::where('from',$request->input('pick_point'))
    ->where('to',$request->input('drop_point'))
    ->first();
    // dd($route);

//how many bus available or not...
    if(!empty($route)){
        $route_id=$route->id;

        $buses=Bus::with('routelocation')->where('route_id',$route_id)
        ->where('coach_type',$request->input('coach_type'))
        ->where('time',$request->input('time'))->get();
    return view('frontend.layouts.show',compact('buses'));
        
    }else{
      return redirect()->back()->with('status','Route is not found.');
    }
    // dd($buses);

    

}
public function ubookingCancle(Request $request)
{
       $validatedData = $request->validate([
           'pick_point' => 'required',
           'drop_point' => 'required',
           'date' => 'required',
           'time' => 'required',
           'coach_type' => 'required',
       ]);

        // dd($request->all());
    $date=$request->date;
    $bookingDate=Session::put('bookingdate',$date);
        //dd($bookingDate);
    
        //$route_id=get route id from route table using Route model, where location from and location to came from user form.
    $buses='';
    $route=Routelocation::where('from',$request->input('pick_point'))
    ->where('to',$request->input('drop_point'))
    ->first();

    if(!empty($route)){
        $route_id=$route->id;

        $buses=Bus::with('routelocation')->where('route_id',$route_id)
        ->where('coach_type',$request->input('coach_type'))->get();
        
    }

    
    return view('frontend.layouts.cancleShow',compact('buses'));
}
public function printTicket()
{
    $userId=auth()->user()->id;
    $data=Reservation::where('user_id', $userId)->with('booking')->with('user')->with('coach')->latest()->get();
    //dd($data);
    return view('frontend.print.printTicket', compact('data'));
}
public function downloadPDF($id)
{
    $downloadPdf=Reservation::with('user')->with('coach')->with('booking')->find($id);

    // dd($downloadPdf);

    $pdf = PDF::loadView('frontend.print.pdfTicket', compact('downloadPdf'));
    // return $pdf->download('invoice.pdf');

    return $pdf->setPaper('a4')->stream();
    //dd($id);
    //$downloadPdf = Reservation::where('id', $id)->with('booking')->with('user')->with('coach')->first();
    //return View('frontend.print.pdfTicket', compact('downloadPdf'));
    //$pdf = PDF::loadView('frontend.print.pdfTicket', compact('downloadPdf'));
    //dd($pdf);
    //dd(PDF::loadView('frontend.print.pdfTicket', compact('downloadPdf')));
    //return $pdf->stream('frontend.print.pdfTicket');
}
public function cancelTkt($id)
{
    $userId=auth()->user()->id;
    $data=Reservation::where('id', $id)->with('booking')->with('user')->with('coach')->first();
    $bookingDate=$data->date;
    //dd($bookingDate);
    $coach_id=$data->coach_id;
    $reserved_seat= array();
    $allSeat=Reservation::where('id','!=', $userId)->where('date', $bookingDate)->where('coach_id', $coach_id)->get();
    foreach ($allSeat as $key => $value) {

        $reserved_seat=array_merge($reserved_seat,json_decode($value->seat));
        //$reserved_seat=json_decode($value->seat);
    }
    //dd($reserved_seat);
    $personalReserved_seat= array();
    $allBookSeats=Reservation::where('user_id', $userId)->where('date', $bookingDate)->where('coach_id', $coach_id)->get();
    foreach ($allBookSeats as $key => $value) {

        $personalReserved_seat=array_merge($personalReserved_seat,json_decode($value->seat));
        //$personalReserved_seat=json_decode($value->seat);
    }
    //dd($personalReserved_seat);
    return view ('frontend.layouts.cancelShowSeat',compact('id','reserved_seat', 'personalReserved_seat', 'data'));
}
public function cancelTktProcess(Request $request)
{
    //dd($request->all());
}

}
