<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\User;

class RegistrationController extends Controller
{


	public function createregistration(Request $request)
	{ 		
		// dd($request->all());

		$data = [
			'name' => $request->input('name'),        
			'email' => $request->input('email'),   
			'password' => bcrypt($request->input('password')),
			'address' => $request->input('address'),    
			'contact_num' => $request->input('contact'),
		];

			User::create($data);
			return redirect()->back()->with('message','User Registration Successfull.');

	}		

}
