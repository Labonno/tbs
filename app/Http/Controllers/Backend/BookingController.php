<?php

namespace App\Http\Controllers\Backend;

use App\models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Booking;

class BookingController extends Controller
{
    public function booking()
    {
    	$data=Reservation::with('booking')->with('user')->latest()->get();
    	return view('backend.booking', compact('data'));
    }

}
