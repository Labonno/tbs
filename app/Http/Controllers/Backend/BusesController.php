<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Bus;
use App\models\Location;
use App\models\Routelocation;
use Illuminate\Support\Facades\Validator;

class BusesController extends Controller
{
    public function buses()
    {
        $time= Routelocation::all();
        // dd($time);
        $routes = Routelocation::all();
    	
        $buses= Bus::all();
    	//dd($buses);
    	return view ('Backend.buses', compact('buses','routes','time'));
    }
  
	public function addBuses(Request $request)
    {
    	//dd($request->all());
        $validatedData = $request->validate([
            'coach_id' => 'required||numeric',
            'reporting' => 'required',
            'route_id' => 'required',
            'depature' => 'required',
            'boarding' => 'required',
            'coach_type' => 'required',
            'time' => 'required',
            'price' => 'required||numeric',
        ]);


        $data = [
    		'coach_id' => $request->input('coach_id'),        
            'reporting' => $request->input('reporting'),
            'route_id' => $request->input('route_id'),   
            'depature' => $request->input('depature'),
            'boarding' => $request->input('boarding'),
            'coach_type' => $request->input('coach_type'),  
            'time'=>$request->input('time'),  
            'price' => $request->input('price'),

    	];
        // dd($data);
    	Bus::create($data);
    	return redirect()->back()->with(['status' => 'Add Coach created successfully']);
    	//return view('backend.buses');
    }
     public function deletebus($id)

    {
        Bus::find($id)->delete();
        return redirect()->back();
    }
 
}
