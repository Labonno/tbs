<?php

namespace App\Http\Controllers\Backend;

use App\models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CustomersController extends Controller
{
    public function customers()
    {
        $datas=User::where('role', 'user')->get();
    	return view ('Backend.customer', compact('datas'));
    }
}
