<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('coach_id')->nullable();
            $table->string('route_id')->nullable();
            $table->time('reporting');
            $table->time('depature');
            $table->string('boarding');
            $table->string('time',16);
            $table->string('coach_type');
            $table->string('price');
            $table->timestamps(); 

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buses');
    }
}
